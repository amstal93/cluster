# cluster

Our Approach to setup a cluster with [Terraform](https://www.terraform.io/) & [k1](https://github.com/kubermatic/kubeone) for provisioning k8s on HCLOUD.

[[_TOC_]]

## Prerequisites

* helm
* helm secrets [https://github.com/jkroepke/helm-secrets](helm-secrets) 
* sops

## Terraform Steps

### Init State

```shell
export GITLAB_PROJECT_ID=25290575
export USERNAME=solidnerd
export GITLAB_TOKEN="<your-personal-access-token>"
```

```shell
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/default" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/default/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/default/lock" \
    -backend-config="username=${USERNAME}" \
    -backend-config="password=${GITLAB_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

### Create and view an execution plan

```bash
# create and view an execution plan
terraform plan
```

### apply the change

```bash
terraform apply -auto-approve
```

### output the apply

```shell
terraform output -json > tf.json
```

## kubeone

### Prepare SSH Agent
Kubeone uses the ssh-agent to connect to the instance. So that your private key works execute

```bash
ssh-add
```

### Apply Kubeone

```bash
kubeone apply -m kubeone.yaml -t tf.json
```

## just
```bash
just cluster-create
```

## Helm

```bash
# Install cert-manager
helm upgrade -i cert-manager ./cert-manager -f ./cert-manager/values.yaml --namespace cert-manager --create-namespace
# Install ClusterIssuer
cat <<EOF | kubectl apply -f -
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt
  namespace: cert-manager
spec:
  acme:
    # You must replace this email address with your own.
    # Let's Encrypt will use this to contact you about expiring
    # certificates, and issues related to your account.
    email: EMAIL
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      # Secret resource that will be used to store the account's private key.
      name: issuer-key
    # Add a single challenge solver, HTTP01 using nginx
    solvers:
      - http01:
          ingress:
            class: nginx
EOF
# Install kube-prometheus-stack
helm secrets upgrade -i kube-prometheus-stack ./helm/kube-prometheus-stack -f ./helm/kube-prometheus-stack/values.yaml -f ./helm/kube-prometheus-stack/secrets.yaml --namespace kube-prometheus-stack --create-namespace
```

## Sops

```shell
find . -type f -name 'secrets.yaml' | xargs -n 1 sops updatekeys --yes
# Decrypt file with SOPS
sops --decrypt FILE
# Encrypt file with SOPS
sops --encrypt FILE
```